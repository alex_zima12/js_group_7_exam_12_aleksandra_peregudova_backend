const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
    title: {
        type: String,
        required: [true, "Поле title обязательно для заполнения"]
    },
    image: {
        type: String,
        required: [true, "Поле image обязательно для заполнения"]
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "Фото автора обязательно"]
    },
});

const Photo = mongoose.model('Photo', PhotoSchema);

module.exports = Photo;
