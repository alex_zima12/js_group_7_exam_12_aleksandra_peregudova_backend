const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");

const Photos = require("./models/Photo");
const User = require("./models/User");

mongoose.connect(config.db.url + "/" + config.db.name, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("photos");
        await db.dropCollection("users");
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }

    const [Petya, Vasya] = await User.create({
        username: "Petya",
        password: "1@345qWert",
        displayName: "display Petya",
        token: nanoid()
    }, {
        username: "Vasya",
        password: "1@345qWert",
        displayName: "display Vasya",
        token: nanoid()
    });

    await Photos.create({
        title: "Beyonce",
        user: Petya._id,
        image: "photo1.png"
    }, {
        title: "Ariana Grande",
        user: Vasya._id,
        image: "photo2.png"
    }, {
        title: "Steven Tyler",
        user: Vasya._id,
        image: "photo3.jpg"
    });

    db.close();
});