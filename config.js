const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "PhotosApp",
    url: "mongodb://localhost"
  },
  fb: {
    appId: "871116873636855",
    appSecret:"32cd9a625eb9ea4440fb6a2e144e36e2"
  }
};

