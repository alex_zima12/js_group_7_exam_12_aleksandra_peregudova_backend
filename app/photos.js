const router = require("express").Router();
const auth = require('../middleware/auth');
const config = require('../config');
const Photo = require('../models/Photo');
const multer = require('multer');
const path = require('path');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const criteria = {};

    if (req.query.user) {
        criteria.user = req.query.user;
    };

    try {
        const photos = await Photo.find(criteria).populate('user', 'displayName');

        return res.send(photos);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    const photoData = {
        title: req.body.title,
        user: req.user._id
    };

    if (req.file) {
        photoData.image = req.file.filename;
    }

    try {
        const photo = new Photo(photoData);

        await photo.save();
        return res.send(photo);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const photo = await Photo.findById(req.params.id);

        if (photo.user.equals(req.user._id)) {
            await photo.remove()

            return res.status(200).send({message: 'Успешно удалено!'});
        } else {
            return res.status(403).send({message: 'Ошибка удаления!'});
        }
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;
